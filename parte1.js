// Implemente uma função que gere uma array de tamanho pré definido pelos parâmetros
// com números aleatórios de 0 a 100 e, em seguida, ordene esse array utilizando um 
// algoritmo de sua preferência. 
// Obs: Implementar algoritmo de ordenação manualmente, sem usar o método sort().


function arrayGenerator(size) {  // gerador de array
    new_array = []
    for(let i = 0; i < size; i++) {
        new_array.push(Math.round(Math.random()*100))
    }

    return new_array
}


function bubbleSort(array) {  // ordenador
    let not_sorted = true  // verificar se a ordenação já terminou
    let size = array.length

    while (not_sorted) {
        not_sorted = false
        size -= 1  // diminui 1 a cada iteração pois os maiores elementos são colocados na posição certa primeiro
        
        for(let i = 0; i < size; i++) {
            let temp = array[i]
            if(array[i] > array[i + 1]) {  // swap
                array[i] = array[i + 1]
                array[i + 1] = temp
                not_sorted = true
            }
        }
    }

    return array
}


v = arrayGenerator(7)
console.log("Array antes = [" + v + ']')
v = bubbleSort(v)
console.log("Array depois = [" + v + ']')
