// Implemente um algoritmo que pegue duas matrizes (array de arrays) e realize sua multiplicação.
// Lembrando que para realizar a multiplicação dessas matrizes o número de linhas da primeira
// matriz tem que ser igual ao número de colunas da segunda matriz. (2x2)
// 	Caso teste 1 : [ [ [2],[-1] ], [ [2],[0] ] ] e  [ [2,3],[-2,1] ] multiplicadas dão  [ [6,5], [4,6] ] 
// 	Caso teste 2 :[[4,0],[-1,-1]] e[[-1,3],[2,7]]  multiplicadas dão  [[-4,12],[-1,-10]] 
	
// *Gente na multiplicação de matrizes eu corrigi os valores do teste 2.


function multiplyMatrices(matrix1, matrix2) {  // multiplica duas matrizes
    let size = matrix1.length
    let matrix_result = []

    for(let i = 0; i < size; i++) {  // alocando matriz resultado
        let row = []
        for(let j = 0; j < size; j++) {
            row.push(0)
        }
        matrix_result.push(row)
    }

    for(let i = 0; i < size; i++) {  // realizando multiplicação estilo "linha de um por coluna do outro"

        for(let j = 0; j < size; j++) {

            for(let k = 0; k < size; k++) {
                matrix_result[i][j] = matrix_result[i][j] + matrix1[i][k] * matrix2[k][j]
            }
        }
    }

    return matrix_result
}


let matriz1_teste1 = [ [ [2],[-1] ], [ [2],[0] ] ]
let matriz2_teste1 = [ [2,3],[-2,1] ]
let res1 = multiplyMatrices(matriz1_teste1, matriz2_teste1)
console.log(res1)

let matriz1_teste2 = [[4, 0], [-1, -1]]
let matriz2_teste2 = [[-1, 3], [2, 7]]
let res2 = multiplyMatrices(matriz1_teste2, matriz2_teste2)
console.log(res2)
